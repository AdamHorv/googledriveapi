/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mmsone.googledrive;

import com.google.api.client.http.FileContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 *
 * @author Adam
 */
public class XlsxHandlerImpl implements XlsxHandler {

    private final DriveStartServices driveStartServices;
    private final Drive service;

    public XlsxHandlerImpl() throws IOException {
        driveStartServices = new DriveStartServices();
        service = driveStartServices.getService();
    }

    @Override
    public void downloadXlsx(String fileId, String xlsxName) throws IOException {
        // Build a new authorized API client service.
        OutputStream outputStream = new ByteArrayOutputStream();
        service.files().export(fileId, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                .executeMediaAndDownloadTo(outputStream);
        ByteArrayOutputStream bos = (ByteArrayOutputStream) outputStream;
        FileOutputStream fos = new FileOutputStream(xlsxName);
        byte[] arr = bos.toByteArray();
        fos.write(arr);
        bos.close();
        fos.close();
        outputStream.close();
    }

    @Override
    public void uploadFile(String reportName, String type, String fileType, String fileContentType) {
        try {
            File fileMetadata = new File();
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
            Date date = new Date();
            fileMetadata.setName(reportName + dateFormat.format(date));
            fileMetadata.setMimeType(type);
            java.io.File filePath = new java.io.File(reportName + fileType);
            FileContent mediContent = new FileContent(fileContentType, filePath);
            service.files().create(fileMetadata, mediContent).execute();
            filePath.delete();
        } catch (IOException ex) {
            Logger.getLogger(XlsxHandlerImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Sheet loadXlsx(String xlsxName) throws FileNotFoundException, IOException, InvalidFormatException {
        InputStream inp = new FileInputStream(xlsxName);
        Workbook wb = WorkbookFactory.create(inp);
        return wb.getSheetAt(0);

    }

    @Override
    public String getStringValue(Sheet sheet, Integer column, Integer rows) {
        Row row = sheet.getRow(rows);
        Cell cell = row.getCell(column);
        try {
            return cell.getStringCellValue();
        } catch (Exception e) {
        }
        return "";
    }
}
