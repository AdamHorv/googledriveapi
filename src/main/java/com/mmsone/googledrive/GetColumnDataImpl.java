/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mmsone.googledrive;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.ss.usermodel.Sheet;

/**
 *
 * @author Adam
 */
public class GetColumnDataImpl extends XlsxHandlerImpl {

    public GetColumnDataImpl() throws IOException {
    }

    private List<String> data;

    /**
     * Get data from a column
     *
     * @param sheet
     * @param header The name of the column
     * @return a List of data
     */
    public List getDataColumn(Sheet sheet, String header) {
        data = new ArrayList<>();
        int i = 0;
        for (int j = 0; !"".equals(getStringValue(sheet, j, i)); j++) {
            if (header.equals(getStringValue(sheet, j, i))) {
                i++;
                for (; !"".equals(getStringValue(sheet, j, i)); i++) {
                    data.add(getStringValue(sheet, j, i));
                }
                break;
            }
        }
        return data;
    }

    /**
     * Get data from a column
     *
     * @param sheet
     * @param header Frist cell in the column
     * @param from
     * @return a List of data
     */
    public List getDataColumn(Sheet sheet, String header, Integer from) {
        data = new ArrayList<>();
        int i = 0;
        for (int j = 0; !"".equals(getStringValue(sheet, j, i)); j++) {
            if (header.equals(getStringValue(sheet, j, i))) {
                i = from;
                for (; !"".equals(getStringValue(sheet, j, i)); i++) {
                    data.add(getStringValue(sheet, j, i));
                }
                break;
            }
        }
        return data;
    }

    /**
     * Get data from a cell
     *
     * @param sheet
     *
     * @param column
     * @param row
     * @return a String of data
     */
    public String getDataColumn(Sheet sheet, Integer column, Integer row) {
        return getStringValue(sheet, column, row);
    }

    /**
     * Get data from a column
     *
     * @param sheet
     * @param header First cell in the column
     * @param from
     * @param to
     * @return a List of data
     */
    public List getDataColumn(Sheet sheet, String header, Integer from, Integer to) {
        data = new ArrayList<>();
        int i = 0;
        for (int j = 0; !"".equals(getStringValue(sheet, j, i)); j++) {
            if (header.equals(getStringValue(sheet, j, i))) {
                i = from;
                for (; !"".equals(getStringValue(sheet, j, i)) && i <= to; i++) {
                    data.add(getStringValue(sheet, j, i));
                }
                break;
            }
        }
        return data;
    }
}
