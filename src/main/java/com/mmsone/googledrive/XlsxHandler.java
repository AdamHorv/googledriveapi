/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mmsone.googledrive;

import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;

/**
 *
 * @author Adam
 */
public interface XlsxHandler {

    /**
     * Download an Xlsx file through DRIVE Api
     *
     * @param fileId Drive file ID
     * @param xlsxName File name (only .xlsx)
     * @throws IOException
     */
    void downloadXlsx(String fileId, String xlsxName) throws IOException;

    /**
     * Upload a new file through DRIVE Api
     *
     * @param reportName file name
     * @param type
     * "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" or
     * "image/png"
     * @param fileType ".xlsx" or ".png"
     * @param fileContentType "text/xlsx" or "image/png"
     */
    void uploadFile(String reportName, String type, String fileType, String fileContentType);

    /**
     * Open an xlsx file from local content.
     *
     * @param xlsxName
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     * @throws InvalidFormatException
     */
    Sheet loadXlsx(String xlsxName) throws FileNotFoundException, IOException, InvalidFormatException;

    /**
     *
     * @param sheet
     * @param column
     * @param rows
     * @return
     */
    String getStringValue(Sheet sheet, Integer column, Integer rows);

    /**
     * Write excetions and where in the xlsx
     *
     * @param exceptions
     * @param where Hol lett dobva az exception?
     * @throws FileNotFoundException
     * @throws IOException
     */
}
